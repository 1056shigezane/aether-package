//
//  HEVCElementaryStream.swift
//  Aether
//
//  Created by 稲田成実 on 2019/01/08.
//  Copyright © 2019 Narumi Inada. All rights reserved.
//

import Foundation
import CoreMedia
import VideoToolbox


/*
class HEVCCompressor : NSObject {

    override init() {
        super.init()
    }

    deinit {
        print("Compressor deinit")
        if compressionSession != nil {
            VTCompressionSessionCompleteFrames(compressionSession!, untilPresentationTimeStamp: CMTime.indefinite)
            VTCompressionSessionInvalidate(compressionSession!)
            compressionSession = nil
        }
    }

    var processSize : (width:Int,height:Int) = (0,0)
    var compressionSession : VTCompressionSession? = nil
    var delegate : H264ComplessionDelegate? = nil




    func compress(imageBuffer pix:CVPixelBuffer, pts: CMTime? = nil){

        let width = CVPixelBufferGetWidth(pix)
        let height = CVPixelBufferGetHeight(pix)
        if processSize.width != width, processSize.height != height {

            createSession(width: width, height: height)//Compression

        }

        if let session = compressionSession {


            let ts = pts ?? CMClockGetTime(CMClockGetHostTimeClock())





            VTCompressionSessionEncodeFrame(session, imageBuffer: pix, presentationTimeStamp: ts, duration: CMTime.invalid, frameProperties: nil, infoFlagsOut: nil, outputHandler: {
                [weak self] (status,flags,sampleBuffer) in

                if let sampleBuffer = sampleBuffer {

                    self?.delegate?.didCompress(sampleBuffer: sampleBuffer)

                }else{

                    print("VTCompressionSession - cb - sampleBuffer is nil,",status,flags)

                }

            })


        }

    }
    var a120FPS : CMTime = CMTime(seconds: 1/120, preferredTimescale: 100000000)
    var invalidateQueue : DispatchQueue = DispatchQueue.init(label: "compressionInvalidateQ")

    func createSession(width:Int,height:Int){
        if compressionSession != nil {
            invalidateQueue.sync {


                VTCompressionSessionCompleteFrames(compressionSession!, untilPresentationTimeStamp: CMTime.indefinite)
                VTCompressionSessionInvalidate(compressionSession!)
                compressionSession = nil
            }
        }
        #if os(iOS)
        let specify = [:] as CFDictionary
        #elseif os(macOS)
        let specify = [
            kVTVideoEncoderSpecification_EnableHardwareAcceleratedVideoEncoder:kCFBooleanTrue,
            //kVTVideoEncoderSpecification_RequireHardwareAcceleratedVideoEncoder:kCFBooleanTrue
            ] as CFDictionary
        #endif

        let attrs = [
            kVTCompressionPropertyKey_MaxKeyFrameInterval:12 as CFTypeRef,
            kVTCompressionPropertyKey_AllowFrameReordering:kCFBooleanFalse!,
            kVTCompressionPropertyKey_MaxFrameDelayCount : 0,
            kVTCompressionPropertyKey_RealTime : kCFBooleanTrue!,
            kVTCompressionPropertyKey_AllowTemporalCompression : kCFBooleanTrue!,
            kVTCompressionPropertyKey_ProfileLevel : kVTProfileLevel_HEVC_Main_AutoLevel
            ] as CFDictionary

        let status = VTCompressionSessionCreate(allocator: kCFAllocatorMalloc, width: Int32(width), height: Int32(height), codecType: kCMVideoCodecType_HEVC, encoderSpecification: specify, imageBufferAttributes: nil, compressedDataAllocator: kCFAllocatorMalloc, outputCallback: nil, refcon: nil, compressionSessionOut: &compressionSession)

        if status != noErr, compressionSession == nil {
            print("VTCompressionSessionCreate fail:",VTErrors[status] ?? "noDesc")
            return
        }

        _ = VTSessionSetProperties(compressionSession!, propertyDictionary: attrs)

        processSize = (width,height)

        //props(session: compressionSession!)

        VTCompressionSessionPrepareToEncodeFrames(compressionSession!)
    }


    private static func bufferToData(sbuf:CMSampleBuffer) -> ([(data:Data,pts:CMTime)],isKeyFrame:Bool) {


        var outputs : [(Data,CMTime)] = []

        var isIFrame:Bool = false

        let pts = CMSampleBufferGetPresentationTimeStamp(sbuf)

        autoreleasepool {
            if let attachmentsArray:CFArray = CMSampleBufferGetSampleAttachmentsArray(sbuf, createIfNecessary: false) {

                if ( CFArrayGetCount(attachmentsArray) > 0 ) {
                    let dict = CFArrayGetValueAtIndex(attachmentsArray, 0)
                    let dictRef:CFDictionary = unsafeBitCast(dict, to: CFDictionary.self)

                    let isDependsOn = CFDictionaryGetValue(dictRef, unsafeBitCast(kCMSampleAttachmentKey_DependsOnOthers, to: UnsafeRawPointer.self))
                    if unsafeBitCast(isDependsOn, to: CFBoolean.self) == kCFBooleanFalse {
                        isIFrame = true
                    }
                }
            }

            let nStartCodeLength:size_t = 4
            let nStartCode:[UInt8] = [0x00, 0x00, 0x00, 0x01]

            if isIFrame {

                var elementaryStream = Data()
                let description:CMFormatDescription = CMSampleBufferGetFormatDescription(sbuf)!

                var numParams:size_t = 0
                CMVideoFormatDescriptionGetHEVCParameterSetAtIndex(description, parameterSetIndex: 0, parameterSetPointerOut: nil, parameterSetSizeOut: nil, parameterSetCountOut: &numParams, nalUnitHeaderLengthOut: nil)


                for i in 0..<numParams {
                    var parameterSetPointer:UnsafePointer<UInt8>? = nil
                    var parameterSetLength:size_t = 0
                    var naluheadertLength:Int32 = 0
                    CMVideoFormatDescriptionGetHEVCParameterSetAtIndex(description, parameterSetIndex: i, parameterSetPointerOut: &parameterSetPointer, parameterSetSizeOut: &parameterSetLength, parameterSetCountOut: nil, nalUnitHeaderLengthOut: &naluheadertLength)

                    elementaryStream.append(nStartCode, count: nStartCodeLength)
                    elementaryStream.append(parameterSetPointer!, count: parameterSetLength)
                }
                outputs.append((elementaryStream,pts))


            }

            var blockBufferLength:size_t = 0
            var bufferDataPointer: UnsafeMutablePointer<Int8>? = nil

            CMBlockBufferGetDataPointer(CMSampleBufferGetDataBuffer(sbuf)!, atOffset: 0, lengthAtOffsetOut: nil, totalLengthOut: &blockBufferLength, dataPointerOut: &bufferDataPointer)


            bufferDataPointer?.withMemoryRebound(to: UInt8.self, capacity: blockBufferLength, {
                p in

                outputs.append(( Data.init(bytes: p, count: blockBufferLength),pts))
            })

        }
        return (outputs,isIFrame)


    }

}















//MARK: - Decomp

class HEVCDecompressor : NSObject {



    var decompressionSession : VTDecompressionSession? = nil
    var formatDesc : CMVideoFormatDescription? = nil
    var delegate : H264DecompressionDelegate? = nil

    public override init() {
        formatDesc = nil
    }

    func streamToFrame(frame : Data,header : Data? = nil) {


        var blockLength = 0

        var nextStartIndex = 0

        var naluType = frame[nextStartIndex + 4]// & 0x1f


        var status : OSStatus = 0



        if naluType != 64, formatDesc == nil {
            //error
            print("nalu is",naluType,", type:",naluTypesStrings[Int(naluType)])
            print(frame)
            return
        }



        var tmpPArray : [[UInt8]] = []


        while naluType == 64 || naluType == 66 || naluType == 68 {

            var parameterSize : Int = 0
            let startIndex = nextStartIndex
            for i in startIndex + 4 ..< startIndex + 100 {

                if i + 3 >= frame.count {

                    parameterSize = frame.count - startIndex - 4
                    nextStartIndex = i + 3
                    naluType = 200

                    break

                }else if frame[i] == 0x00 && frame[i+1] == 0x00 && frame[i+2] == 0x00 && frame[i+3] == 0x01 {

                    parameterSize = i - startIndex - 4
                    nextStartIndex = i
                    naluType = frame[nextStartIndex + 4]
                    break

                }
            }


            var parameter = [UInt8].init(repeating: 0, count: parameterSize)
            frame.copyBytes(to: &parameter, from: startIndex + 4 ..< nextStartIndex )

            tmpPArray.append(parameter)


            if nextStartIndex >= frame.count {

                formatDesc = nil

                var dataParamArray : [UnsafePointer<UInt8>] = []
                var sizeParamArray : [Int] = []
                for v in tmpPArray {

                    dataParamArray.append(UnsafePointer<UInt8>(v))
                    sizeParamArray.append(v.count)
                }

                status = CMVideoFormatDescriptionCreateFromHEVCParameterSets(allocator: kCFAllocatorMalloc, parameterSetCount: dataParamArray.count, parameterSetPointers: dataParamArray, parameterSetSizes: sizeParamArray, nalUnitHeaderLength: 4, extensions: nil, formatDescriptionOut: &formatDesc)



                tmpPArray = []
                if status != noErr {

                    print("Format Desc Create Error",status)
                    return
                }

                var needNewDecomSession = false
                if decompressionSession != nil && formatDesc != nil {
                    needNewDecomSession = VTDecompressionSessionCanAcceptFormatDescription(decompressionSession!, formatDescription: formatDesc!) == false
                }else if decompressionSession == nil {
                    needNewDecomSession = true
                }

                if needNewDecomSession {
                    createDecompSession()
                }

                return
            }
        }


        if /*naluType == 2 || naluType == 40,*/ formatDesc != nil {//IDR

            var blockBuffer : CMBlockBuffer? = nil

            blockLength = frame.count


            status = frame.withUnsafeBytes({
                //(p : UnsafePointer<UInt8>) in
                ptr in
                let p = UnsafeMutableRawPointer(mutating: ptr.baseAddress)
                return CMBlockBufferCreateWithMemoryBlock(allocator: nil, memoryBlock: UnsafeMutableRawPointer(mutating: p), blockLength: blockLength, blockAllocator: kCFAllocatorNull, customBlockSource: nil, offsetToData: 0, dataLength: blockLength, flags: 0, blockBufferOut: &blockBuffer)

            })

            var sampleBuffer : CMSampleBuffer? = nil

            if status == noErr {

                var ts = CMClockGetTime(CMClockGetHostTimeClock())
                if let header = header   {
                    if header.count >= MemoryLayout<Double>.size {

                        ts = header.withUnsafeBytes { ptr in
                            let p = UnsafeMutableRawPointer(mutating: ptr.baseAddress)
                            return p!.load(as:Double.self).cmTime
                        }
//                        ts = header.withUnsafeBytes{
//                            ptr in
//                            return ptr.baseAddress.load(Double.self).cmTime
//
//                        }
                    }
                }

                let sampleSize = [blockLength]
                var timing : [CMSampleTimingInfo] = [CMSampleTimingInfo.init(duration: CMTime.invalid, presentationTimeStamp: ts, decodeTimeStamp: CMTime.invalid)]

                status = CMSampleBufferCreateReady(allocator: kCFAllocatorMalloc, dataBuffer: blockBuffer, formatDescription: formatDesc, sampleCount: 1, sampleTimingEntryCount: 1, sampleTimingArray: &timing[0], sampleSizeEntryCount: 1, sampleSizeArray: sampleSize, sampleBufferOut: &sampleBuffer)

            }else{
                print("~~~~~~~ Received NALU Type \"%@\" ~~~~~~~~", naluTypesStrings[Int(naluType)])
                return
            }

            if status == noErr, decompressionSession != nil {

                VTDecompressionSessionDecodeFrame(decompressionSession!, sampleBuffer: sampleBuffer!, flags: [._EnableAsynchronousDecompression,._EnableTemporalProcessing,._1xRealTimePlayback], infoFlagsOut: nil, outputHandler: {

                    [weak self] (osstatus,infoflags,imageBuffer,pts,dts) in
                    Thread.setThreadPriority(1.0)

                    if imageBuffer != nil {
                        self?.delegate?.didDecompress(imageBuffer: imageBuffer!,pts: pts)
                    }



                })



            }
        }


        // }



    }





    func createDecompSession(){
        print("createDecompressionSession")

        if decompressionSession != nil {
            VTDecompressionSessionFinishDelayedFrames(decompressionSession!)
            VTDecompressionSessionWaitForAsynchronousFrames(decompressionSession!)
            VTDecompressionSessionInvalidate(decompressionSession!)
            decompressionSession = nil
        }



        let destinationPixelBufferAttributes = [kCVPixelBufferMetalCompatibilityKey:kCFBooleanTrue] as CFDictionary


        #if os(iOS)
        let specify = [:] as CFDictionary
        #elseif os(macOS)
        NSLog("elseif os(macOS)")
        let specify = [
            kVTVideoDecoderSpecification_EnableHardwareAcceleratedVideoDecoder:kCFBooleanTrue,
            ] as CFDictionary
        #endif

        let status = VTDecompressionSessionCreate(allocator: kCFAllocatorMalloc, formatDescription: formatDesc!, decoderSpecification: specify, imageBufferAttributes: destinationPixelBufferAttributes, outputCallback: nil, decompressionSessionOut: &decompressionSession)

        NSLog("Video Decompression Session Create: \t %@", (status == noErr) ? "successful!" : "failed...")

        if status != noErr, decompressionSession == nil {
            return
        }

        //props(session: decompressionSession!)

        let attrs = [kVTDecompressionPropertyKey_RealTime : kCFBooleanTrue,] as CFDictionary

        VTSessionSetProperties(decompressionSession!, propertyDictionary: attrs)


    }

    func setQualityOfService(){



        let tier1 = [kVTDecompressionPropertyKey_OnlyTheseFrames:kVTDecompressionProperty_OnlyTheseFrames_AllFrames,
                     kVTDecompressionPropertyKey_ReducedFrameDelivery:1.0 as CFNumber ] as CFDictionary
        let tier2 = [kVTDecompressionPropertyKey_OnlyTheseFrames:kVTDecompressionProperty_OnlyTheseFrames_AllFrames,
                     kVTDecompressionPropertyKey_ReducedFrameDelivery:0.5 as CFNumber ] as CFDictionary
        let tier3 = [kVTDecompressionPropertyKey_OnlyTheseFrames:kVTDecompressionProperty_OnlyTheseFrames_IFrames,
                     kVTDecompressionPropertyKey_ReducedFrameDelivery:0.25 as CFNumber ] as CFDictionary
        let qos = [kVTDecompressionPropertyKey_SuggestedQualityOfServiceTiers:[tier1,tier2,tier3]] as CFDictionary


        let st = VTSessionSetProperties(decompressionSession!, propertyDictionary: qos)
        print("DecompQOSSet",VTErrors[st] ?? "err not found.")

        var p : CFDictionary? = nil

        print(VTSessionCopySupportedPropertyDictionary(decompressionSession!, supportedPropertyDictionaryOut: &p)
            == noErr ? "GotQOS" : "GetQOSErr")

        print(p ?? "nilでした")
    }

    deinit {
        print("Decompressor deinit")
        formatDesc = nil
        if decompressionSession != nil {
            VTDecompressionSessionFinishDelayedFrames(decompressionSession!)
            VTDecompressionSessionWaitForAsynchronousFrames(decompressionSession!)
            VTDecompressionSessionInvalidate(decompressionSession!)
            decompressionSession = nil
        }
    }
}
*/

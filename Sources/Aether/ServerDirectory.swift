//
//  ServerDirectory.swift
//  Vapor
//
//  Created by 稲田成実 on 2018/07/30.
//  Copyright © 2018 Narumi Inada. All rights reserved.
//

import Foundation
import Network

#if os(iOS)
import UIKit
#endif

let serviceType = "_videoserver._tcp."
//let serviceTypeV2 = "_videoserverV2._tcp."

public class ServerDirectory : NSObject {
    
    public static let shared = ServerDirectory()
    
    public var foundServices : [NetService] = []
    public var rememberedServices : [String] = {
        (UserDefaults.standard.array(forKey: "RememberedServices" ) as? [String] ) ?? []
    }() {
        didSet{
            UserDefaults.standard.setValue(rememberedServices, forKey: "RememberedServices")
        }
    }
    
    private var resolivingServices : [NetService] = []
    
    public static let ipAddress : [String] = getIFAddresses()
    public static func isSameIP(service:NetService) -> Bool {
        
        
        for name in service.hostNames {
            if ipAddress.contains(name) {
                return true
            }
        }
        
        return false
    
    }
    
    public static let serverDirectoryDidChangeNotificationName : Notification.Name = Notification.Name("ServerDirectoryDidChange")
    public static let serverDirectoryDidChangeNotification : Notification = Notification.init(name: ServerDirectory.serverDirectoryDidChangeNotificationName)
    
    private var browser : NetServiceBrowser!
    
    private override init(){
        super.init()
        
        restart()
        #if os(iOS)
        
        NotificationCenter.default.addObserver(self, selector: #selector(restart), name: UIApplication.willEnterForegroundNotification, object: nil)
        
        #endif
    }
    
    @objc func restart(){
        browser = NetServiceBrowser()
        browser.delegate = self
        self.browser.schedule(in: .main, forMode: RunLoop.Mode.common)
        self.browser.searchForServices(ofType: serviceType, inDomain: "local.")
    }
    
    deinit {
        #if os(iOS)
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
        #endif
        browser.remove(from: .main, forMode: RunLoop.Mode.common)
        browser.stop()
        browser.delegate = nil
    }
    
//    func createClientFromRememberedServices() -> VideoClient {
//
//    }
    
}

extension ServerDirectory : NetServiceBrowserDelegate {
    
    
    public func netServiceBrowser(_ browser: NetServiceBrowser, didFind service: NetService, moreComing: Bool) {
       
        
        if !foundServices.contains(service) {
            
            print("Browser find addresses",service.addresses ?? "No address")
            service.delegate = self
            
            service.resolve(withTimeout: 10.0)
            resolivingServices.append(service)
           
        }
    }
    
    public func netServiceBrowser(_ browser: NetServiceBrowser, didRemove service: NetService, moreComing: Bool) {
        print("ServerDidRemove")
        
        if resolivingServices.contains(service) {
            resolivingServices.remove(at: resolivingServices.firstIndex(of: service)!)
        }
        
        if foundServices.contains(service) {
            foundServices.remove(at: foundServices.firstIndex(of: service)!)
            NotificationCenter.default.post(ServerDirectory.serverDirectoryDidChangeNotification)
            print("ÆDir.ServerDir: " + service.name + " removed")
        }
        
        
    }
    
    
}

extension NetService {
    var hostNames : [String] {
        var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
        guard let addresses = self.addresses else {
            print("NetService: No addresses")
            return [] }
        
        
        var tmp : [String] = []
        
        for data in addresses {
            
            _ = data.withUnsafeBytes{
                getnameinfo($0.bindMemory(to: sockaddr.self).baseAddress, socklen_t($0.count), &hostname, socklen_t(hostname.count), nil, 0, NI_NUMERICHOST)
            }
            tmp.append(String(cString:hostname))
        }
        
        return tmp
    }
}

extension ServerDirectory : NetServiceDelegate {
    public func netServiceDidResolveAddress(_ sender: NetService) {
        
        if resolivingServices.contains(sender) {
            foundServices.append(resolivingServices.remove(at: resolivingServices.firstIndex(of: sender)!))
            print("ÆDir.foundServerAdded,count:\(foundServices.count)")
            if !rememberedServices.contains(sender.name) {
                rememberedServices.append(sender.name)
            }
            
            NotificationCenter.default.post(ServerDirectory.serverDirectoryDidChangeNotification)
        }
        
        
        
    }
    
    public func netService(_ sender: NetService, didNotResolve errorDict: [String : NSNumber]) {
        
        if resolivingServices.contains(sender) {
            foundServices.append(resolivingServices.remove(at: resolivingServices.firstIndex(of: sender)!))
            print("ÆDir.foundServerAdded,count:\(foundServices.count)")
            
            if !rememberedServices.contains(sender.name) {
                rememberedServices.append(sender.name)
            }
            NotificationCenter.default.post(ServerDirectory.serverDirectoryDidChangeNotification)
        }
        
    }
    
    public func netServiceWillResolve(_ sender: NetService) {
        print("netServiceWillResolve")
    }
    
    static func getIFAddresses() -> [String] {
        var addresses = [String]()
        
        // Get list of all interfaces on the local machine:
        var ifaddr : UnsafeMutablePointer<ifaddrs>?
        guard getifaddrs(&ifaddr) == 0 else { return [] }
        guard let firstAddr = ifaddr else { return [] }
        
        // For each interface ...
        for ptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
            let flags = Int32(ptr.pointee.ifa_flags)
            let addr = ptr.pointee.ifa_addr.pointee
            
            // Check for running IPv4, IPv6 interfaces. Skip the loopback interface.
            if (flags & (IFF_UP|IFF_RUNNING|IFF_LOOPBACK)) == (IFF_UP|IFF_RUNNING) {
                if addr.sa_family == UInt8(AF_INET) || addr.sa_family == UInt8(AF_INET6) {
                    
                    // Convert interface address to a human readable string:
                    var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                    if (getnameinfo(ptr.pointee.ifa_addr, socklen_t(addr.sa_len), &hostname, socklen_t(hostname.count),
                                    nil, socklen_t(0), NI_NUMERICHOST) == 0) {
                        let address = String(cString: hostname)
                        addresses.append(address)
                        if addr.sa_family == UInt8(AF_INET) {
                            print("ipv4 ### \(address)")
                        }
                        
                        if addr.sa_family == UInt8(AF_INET6) {
                            print("ipv6 ### \(address)")
                        }
                    }
                }
            }
        }
        
        freeifaddrs(ifaddr)
        return addresses
    }
    
    
}

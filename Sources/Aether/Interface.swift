//
//  Interface.swift
//  Vapor
//
//  Created by 稲田成実 on 2018/07/30.
//  Copyright © 2018 Narumi Inada. All rights reserved.
//

import Foundation
import CoreVideo
import CoreMedia
import VideoToolbox
import Network

#if os(iOS)
import UIKit
#endif

let interval = 1.0 / 60.0
let bufSize = 65536

let threadPriority  = 1.0

var trackingMode : RunLoop.Mode {.common}

var performModes : [RunLoop.Mode] = [.default,.common]



typealias Compressor = H264Compressor
typealias Decompressor = H264Decompressor
 /*
typealias Compressor = HEVCCompressor
typealias Decompressor = HEVCDecompressor
*/


public protocol CompressedBufferCallback : AnyObject {
    func compressed(compressedBuffer:CMSampleBuffer)
}

public protocol H264ComplessionDelegate : AnyObject {
    func didCompress(sampleBuffer:CMSampleBuffer)
}

public protocol H264DecompressionDelegate : AnyObject {
    func didDecompress(imageBuffer:CVImageBuffer,pts:CMTime)
}

public protocol VideoClientDelegate {
    /**
     Frame received callback
     Should call be in main?
     
     Now, called in main Thread.
     For IOSurface, videoclient release IOSurface immediately after this callback.
     
     */
    func newFrameCallBack(imageBuffer:CVImageBuffer)
    func serverRetired(client:VideoClient)
}

typealias FrameData = (data:Data,pts:CMTime,isParameterSet:Bool)

//MARK: - Server

public final class VideoServer : NSObject, NetServiceDelegate, H264ComplessionDelegate {
    
    
    var clients : [AetherStreamDelegate:[FrameData]] = [:]
    
    var compressor : Compressor!
    var service : NetService!
    let name : String
    
    public weak var delegate : CompressedBufferCallback? = nil
    public var needsCompressionForRecording : Bool = false
    
    private var lastRequestedTime : Double = Date.timeIntervalSinceReferenceDate - 15.0
    private var needsCompression : Bool { get{ return ( Date.timeIntervalSinceReferenceDate - lastRequestedTime ) < 5 } }
    var frameDropCount : Int = 0
    
    
    var running : Bool = false
    weak var runloop : RunLoop!
    var exited : Bool = false
    
    
   
    
    public var averageBitrate : Int {
        get{
           return compressor.averageBitrate
        }
        set(v){
            compressor.averageBitrate = v
        }
    }
    
    
    
    
    public init(name:String,delegate del:CompressedBufferCallback? = nil ) {
        
        self.name = name
        delegate = del
        
        super.init()
        
        #if os(iOS)
        
        NotificationCenter.default.addObserver(forName: UIApplication.willEnterForegroundNotification, object: nil, queue: .main) { (note) in
            print("VideoServer : willEnterForegroundNotification",note)
            
            self.notifyEnterForegrund {
                guard case .invalid = self.state else{ return }
                
                self.restart()
            }
            
        }

        NotificationCenter.default.addObserver(forName: UIApplication.didEnterBackgroundNotification, object: nil, queue: .main) { (note) in
            print("VideoServer : didEnterBackgroundNotification",note)
            self.stop()
        }

        
        #endif
       
        restart()
    }
    
    #if os(iOS)
    func notifyEnterForegrund(blk:@escaping ()->Void){
        if UIApplication.shared.applicationState == .active {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            blk()
            }
        }else{
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.notifyEnterForegrund(blk: blk)
            }
        }
    }
    #endif
    
    
    deinit{
        print("VideoServer deinit")
        stop()
        compressor.delegate = nil
        
    }
    
    
    enum State {
        case invalid
        case pending
        case running
    }
    
    var state : State = .invalid
    
    
    public func restart(){
        print("VideoServer Restart")
        
        guard state != .pending else{return}
        
        state = .pending
        service = NetService(domain: "local.", type: serviceType, name: name)
        service.delegate = self
        compressor = Compressor(delegate:self)
        clients = [:]
        
        
         
        
        Thread {
            [weak self] in
            if let self = self {
                
                Thread.current.name = "aether_server:" + self.name
                Thread.setThreadPriority(threadPriority)
                
                
                self.runloop = RunLoop.current
                
                self.service.schedule(in: self.runloop, forMode: trackingMode)
                self.service.publish(options: .listenForConnections)
                

                
                ///ThreadLoop
                self.exited = false
                
                self.state = .running
                print("Start Video Server:",self.name)

                RunLoop.current.run()
                self.state = .invalid
                print((Thread.current.name ?? "No Name Thread") + " isExitting...")
            }
        }.start()
        
        
        lastRequestedTime = Date.timeIntervalSinceReferenceDate - 15
    }
    
    
    public func stop(){
        print("stop")
        delegate = nil
        guard !exited else{ return }
        exited = true
        
        let blk = {
            for c in self.clients {
                c.key.close()
            }
            
            self.clients.removeAll()
            
            
            self.service.remove(from: self.runloop, forMode: trackingMode)
            
            CFRunLoopStop(self.runloop.getCFRunLoop())
            
            self.service.delegate = nil
            self.service.stop()
            self.service = nil
        }
        
        if let runloop = runloop {
            runloop.perform{
                
                blk()
                
            }
        }else{
            blk()
        }
        
        
        
        compressor.delegate = nil
        
        
        
    }
    
    
    
    
    public func publish(sbuf:CMSampleBuffer, pts:CMTime? = nil){
        
        if let pix = CMSampleBufferGetImageBuffer(sbuf){
            publish(pixelBuffer: pix, pts: pts)
        }
        
    }
    
   //let ioHeader : PublishHeader = try! PublishHeader(version: 1.0, isIOSurface: true, pts: 0.0, codec: .h264)
    
    let ioHeader : PublishHeader = try! PublishHeader(version: 1.0, isIOSurface: true, pts: 0.0, codec: .h264)
    
    
    public func publish(pixelBuffer:CVPixelBuffer, pts: CMTime? = nil){
        
        guard runloop != nil else{return}
        
        runloop.perform {
            
            
            let clientsInDevice : [AetherStreamDelegate] = self.clients.compactMap { arg in
                if arg.key.isSameIP && arg.key.requesting { return arg.key }
                else{return nil}
            }
            
            if !clientsInDevice.isEmpty,
               let ref = CVPixelBufferGetIOSurface(pixelBuffer)?.takeUnretainedValue() {
                IOSurfaceIncrementUseCount(ref)
                let id = IOSurfaceGetID(ref)
                
                clientsInDevice.forEach { ae in
                    ae.requesting = false
                    ae.send(header: self.ioHeader.data, data: id.data)
                }
                
                IOSurfaceDecrementUseCount(ref)
            }
            
//            if let ref = CVPixelBufferGetIOSurface(pixelBuffer)?.takeUnretainedValue() {
//                IOSurfaceIncrementUseCount(ref)
//                let id = IOSurfaceGetID(ref)
//
//
//
//                for client in self.clients {
//                    let ae = client.key
//                    if ae.isSameIP && ae.requesting {
//                        ae.requesting = false
//                        ae.send(header: self.ioHeader.data, data: id.data)
//                    }
//                }
//
//
//                IOSurfaceDecrementUseCount(ref)
//            }
            
            if self.needsCompression || self.needsCompressionForRecording {
                self.compressor.compress(imageBuffer: pixelBuffer, pts:pts)
            }
            
        }
    }
    
    
    
    public func netService(_ sender: NetService,
                    didNotPublish errorDict: [String : NSNumber]){
        print(errorDict)
    }
    
    public func netService(_ sender: NetService, didAcceptConnectionWith inputStream: InputStream, outputStream: OutputStream) {
        
        print("Server_didAcceptConnectionWith",sender.name)
        let ae = AetherStreamDelegate(input: inputStream, output: outputStream, delegate: self)
        runloop.perform {
            self.clients[ae] = []
        }
        
    }
    
    
    public func didCompress(sampleBuffer: CMSampleBuffer) {
        DispatchQueue.main.async {
            self.delegate?.compressed(compressedBuffer: sampleBuffer)
        }
        if needsCompression {
            let frameData = H264Compressor.bufferToData(sbuf: sampleBuffer,isHEVC: false)
            
            
            runloop.perform {
                
                for key in self.clients.keys {
                    
                    if frameData.isKeyFrame {
                        
                        if self.clients[key]?.count ?? 0 > 6 {
                            self.frameDropCount += self.clients[key]!.count
                            print("Frame dropped : \(self.frameDropCount), requesting:",key.requesting)
                            self.clients[key]?.removeAll()
                        }
                    }
                    
                    if !key.isSameIP {
                        self.clients[key]?.append(contentsOf: frameData.data)
                        self.pushQueue(key: key)
                    }
                    
                }
            }
        }
    }
    
 
    
    func pushQueue(key:AetherStreamDelegate){
        
        guard clients[key] != nil,
              !(clients[key]?.isEmpty ?? true),
              key.requesting,
              !key.isProcessing
        else{return}
        
        
        key.requesting = false
        
        
        autoreleasepool {
            
            if let frame = clients[key]?.remove(at: 0) {
                
                switch key.version {
                
                case 0.1:
                    
                    let hevcHeader = try! PublishHeader(version: 1.0, isIOSurface: false, pts: frame.pts.seconds,codec: .h264)
                    key.send(header: hevcHeader.data, data: frame.data)
                    
                case 1.0 ..< 2.0:
                    let hevcHeader = try! PublishHeader(version: 1.0, isIOSurface: false, pts: frame.pts.seconds,codec: .h264)
                    key.send(header: hevcHeader.data, data: frame.data)
                    
                    break
                    
                    
                default:
                    key.send(header: frame.pts.seconds.data, data: frame.data)
                    break
                    
                }
            }
            
        }
        
        
    }
}

extension VideoServer : AetherStreamCompletionDelegate {
    
    func outputComplete(sender: AetherStreamDelegate) {
        
        switch sender.version {
        
        case 0.1:
            
            if !sender.isProcessing, let frame = clients[sender]?.remove(at: 0) {
                let hevcHeader = try! PublishHeader(version: 1.0, isIOSurface: false, pts: frame.pts.seconds,codec: .h264)
                sender.send(header: hevcHeader.data, data: frame.data)
            }
            
            break
            
        
            
        default:
            break
            
        }
        
    }
    
    func inputComplete(sender: AetherStreamDelegate, data: Data, header: Data, hasContinuousLoad:Bool) {
        
        
        if let requestHeader = try? RequestHeader(from: header) {
            
            runloop.perform {
                self.lastRequestedTime = Date.timeIntervalSinceReferenceDate
                sender.setParams(requesting: true, isSameIP: requestHeader.isSameIP,version: requestHeader.version)
                
                switch requestHeader.version {
                
                case 0.1:
                    if !requestHeader.isSameIP {
                        self.pushQueue(key: sender)
                    }
                
                case 1.0 ..< 2.0:
                    
                    if !requestHeader.isSameIP {
                        self.pushQueue(key: sender)
                    }
                    
                    
                    
                    break
                default:
                    break
                }
                
            }
            
        }else{
            
            runloop.perform {
                guard self.clients[sender] != nil  else{ return }
                
                sender.setParams(requesting: true, isSameIP: false, version: 0.0)
                sender.requesting = true
                self.pushQueue(key: sender)
            }
            
        }
    }
    
    func inputInComplete(sender: AetherStreamDelegate) {
        print("Video Server Input InComplete." )
    }
    
    func errorOrCloseOccured(sender: AetherStreamDelegate) {
        runloop.perform {
            sender.close()
            self.clients.removeValue(forKey: sender)
        }
    }
    
    
}












//MARK: - Client

func getRandStr() -> String {
    let letter = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","1","2","3","4","5","6","7","8","9","0"]
    var str = ""
    for _ in 0 ..< 10 { str += letter[Int(arc4random_uniform(UInt32(letter.count - 1)))] }
    return str
}





public class VideoClient : NSObject, NetServiceDelegate, H264DecompressionDelegate {
    
    
    public var delegate : VideoClientDelegate? = nil
    
    var decompressor : Decompressor = Decompressor()
    
    var isSameIP : Bool = false
    var requestHeader : RequestHeader?
    var streamDelegate : AetherStreamDelegate!
    
    var name : String!
    var server : NetService!
    
    var lastRequestTime : Double = 0.0
    var timer : DispatchSourceTimer!
    
    
    
    enum Status : UInt8 {
        case running = 0
        case stopping = 1
        case waitingForReconnect
    }
    
    var status : Status = .stopping
    
    public var serverName : String { get{ return server.name } }
    public func didDecompress(imageBuffer: CVImageBuffer,pts:CMTime) {
        
        
        CVPixelBufferLockBaseAddress(imageBuffer, .readOnly)
        
        let blk = {
            self.delegate?.newFrameCallBack(imageBuffer: imageBuffer)
            CVPixelBufferUnlockBaseAddress(imageBuffer, .readOnly)
        }
        
        if Thread.current == .main {
            blk()
        }else{
            DispatchQueue.main.sync{ blk() }
        }
        
        
    }
    

    
    
    public init(service s: NetService, delegate:VideoClientDelegate? = nil) {
        super.init()
        
        decompressor.delegate = self
        name = getRandStr()
        server = s
      //  isSameIP = ServerDirectory.isSameIP(service: server)
        
        requestHeader = RequestHeader(version: 1.0, isSameIP: isSameIP)
        
        self.delegate = delegate
        
        var input : InputStream? = nil
        var output : OutputStream? = nil
        
        server.getInputStream(&input, outputStream:&output)
        
        
        streamDelegate = AetherStreamDelegate(input: input!, output: output!, delegate: self)
        
        NotificationCenter.default.addObserver(self, selector: #selector(serverDirectoryDidChange), name: ServerDirectory.serverDirectoryDidChangeNotificationName, object: nil)
        
        start()
    }
    
    deinit {
        print("deinitClient:",name ?? "No Name")
        NotificationCenter.default.removeObserver(self, name: ServerDirectory.serverDirectoryDidChangeNotificationName, object: nil)
        stop()
    }
    
    
    
    
    func start(){
        request()
        
        timer = DispatchSource.makeTimerSource(flags: .init(rawValue: 0), queue: .main)
        timer.schedule(deadline: .now(), repeating: 1 / 4.0)
        timer.setEventHandler(handler: {
            self.timerFunc()
        })
        timer.resume()
        
        lastRequestTime = Date.timeIntervalSinceReferenceDate
        status = .running
    }
    
    public func stop(){
        
        guard status != .stopping else{return}
        
        status = .stopping
        print("VideoClient: stopped " + server.name)
        
        
        timer.cancel()
        
        decompressor.delegate = nil
        streamDelegate.close()
        streamDelegate = nil
        
        
        RunLoop.main.perform {
            self.delegate?.serverRetired(client: self)
        }
        
        
        
        print("closing...:")
        
        
        switch status {
        case .running:
            break
        case .stopping:
            break
        case .waitingForReconnect:
            break
        }
        
        
        
        
    }
    
    
    
    
    func errorOrCloseOccured(st: InputStream) { //InputCompletionDelegate
        print("VideoClient\t: errorOrClose ")
        stop()
    }
    
    
    @objc func serverDirectoryDidChange(note:Notification) {
        
        let sd = ServerDirectory.shared
        
        if !sd.foundServices.contains(server) {
            print("serverDirectoryDidChange not contains")
            stop()
        }
    }
    
    
    @objc func timerFunc(){
        
        let tempDuration = Date.timeIntervalSinceReferenceDate - lastRequestTime
        if tempDuration > 1.0 {
            print("timerRequest")
            request()
        }
        
    }
    
    
    
    
    private func request(){
        
        guard streamDelegate != nil else{return}
        
        if !streamDelegate.isProcessing {
            let nm = name.data(using: .utf8)!
            streamDelegate.send(header: requestHeader?.data ?? Data(), data: nm)
        }
        lastRequestTime = Date.timeIntervalSinceReferenceDate
    }
    
}




extension VideoClient : AetherStreamCompletionDelegate {
    
    func outputComplete(sender: AetherStreamDelegate) {}

    func inputComplete(sender: AetherStreamDelegate, data: Data, header h: Data, hasContinuousLoad:Bool) {
        
        print("Input Complete")
        guard !data.isEmpty else{
            print("Data Empty")
            return }
        
        if let header = try? PublishHeader(from: h) {
            
            switch header.version {
            
            case 0.1:
                if !hasContinuousLoad { request() }
                
                if header.isIOSurfaceID {
                    
                    
                    let id = IOSurfaceID(from: data)
                    
                    if let ref = IOSurfaceLookup(id) {
                        
                        var ptr : Unmanaged<CVPixelBuffer>? = nil
                        
                        IOSurfaceIncrementUseCount(ref)
                        CVPixelBufferCreateWithIOSurface(kCFAllocatorDefault, ref, nil, &ptr)
                        
                        if let cv = ptr?.takeRetainedValue() {
                            
                            DispatchQueue.main.async {
                                self.delegate?.newFrameCallBack(imageBuffer: cv)
                                
                                IOSurfaceDecrementUseCount(ref)
                            }
                        }else{
                            IOSurfaceDecrementUseCount(ref)
                        }
                    }
                    
                    
                }else{
                    
                    DispatchQueue.global().async {
                        
                         
                            self.decompressor.streamToFrame(frame: data, pts: header.pts?.cmTime)
                            
                        
                    }
                }
                
                break
                
            case 1.0 ..< 2.0:
                request()
                
                if header.isIOSurfaceID {
                    
                    
                    let id = IOSurfaceID(from: data)
                    
                    if let ref = IOSurfaceLookup(id) {
                        
                        var ptr : Unmanaged<CVPixelBuffer>? = nil
                        
                        IOSurfaceIncrementUseCount(ref)
                        CVPixelBufferCreateWithIOSurface(kCFAllocatorDefault, ref, nil, &ptr)
                        
                        if let cv = ptr?.takeRetainedValue() {
                            
                            DispatchQueue.main.async {
                                self.delegate?.newFrameCallBack(imageBuffer: cv)
                                
                                IOSurfaceDecrementUseCount(ref)
                            }
                        }else{
                            IOSurfaceDecrementUseCount(ref)
                        }
                    }
                    
                    
                }else{
                    
                    DispatchQueue.global().async {
                        
                        self.decompressor.streamToFrame(frame: data, pts: header.pts?.cmTime)
                        
                    }
                }
                
                
                break
            default :
                break
            }
            
        }else{
            
            request()
            
            var ts : CMTime?
            if h.count >= MemoryLayout<Double>.size {
                ts = h.withUnsafeBytes({
                    return $0.load(as: Double.self).cmTime
                })
            }
            
            self.decompressor.streamToFrame(frame: data, pts:ts)
            
        }
    }
    
    func inputInComplete(sender: AetherStreamDelegate) {
        request()
    }
    
    func errorOrCloseOccured(sender: AetherStreamDelegate) {
        print("VideoClient errorOrCloseOccured")
        stop()
    }
    
    
}








let VTErrors : [OSStatus:String] = [
    noErr : "Success!",
    kVTPropertyNotSupportedErr  :      "kVTPropertyNotSupportedErr"       ,
    kVTPropertyReadOnlyErr        :"kVTPropertyReadOnlyErr"           ,
    kVTParameterErr     :"kVTParameterErr"                     ,
    kVTInvalidSessionErr       : "kVTInvalidSessionErr"              ,
    kVTAllocationFailedErr               :"kVTAllocationFailedErr"   ,
    kVTPixelTransferNotSupportedErr     :"kVTPixelTransferNotSupportedErr"     , // c.f. -8961
    kVTCouldNotFindVideoDecoderErr    :"kVTCouldNotFindVideoDecoderErr"       ,
    kVTCouldNotCreateInstanceErr     :"kVTCouldNotCreateInstanceErr"        ,
    kVTCouldNotFindVideoEncoderErr   :"kVTCouldNotFindVideoEncoderErr"        ,
    kVTVideoDecoderBadDataErr       :"kVTVideoDecoderBadDataErr"         , // c.f. -8969
    kVTVideoDecoderUnsupportedDataFormatErr :"kVTVideoDecoderUnsupportedDataFormatErr" , // c.f. -8970
    kVTVideoDecoderMalfunctionErr           :"kVTVideoDecoderMalfunctionErr" , // c.f. -8960
    kVTVideoEncoderMalfunctionErr           :"kVTVideoEncoderMalfunctionErr" ,
    kVTVideoDecoderNotAvailableNowErr       :"kVTVideoDecoderNotAvailableNowErr" ,
    kVTImageRotationNotSupportedErr         :"kVTImageRotationNotSupportedErr" ,
    kVTVideoEncoderNotAvailableNowErr       :"kVTVideoEncoderNotAvailableNowErr" ,
    kVTFormatDescriptionChangeNotSupportedErr  :"kVTFormatDescriptionChangeNotSupportedErr" ,
    kVTInsufficientSourceColorDataErr       :"kVTInsufficientSourceColorDataErr" ,
    kVTCouldNotCreateColorCorrectionDataErr :"kVTCouldNotCreateColorCorrectionDataErr" ,
    kVTColorSyncTransformConvertFailedErr   :"kVTColorSyncTransformConvertFailedErr" ,
    kVTVideoDecoderAuthorizationErr         :"kVTVideoDecoderAuthorizationErr" ,
    kVTVideoEncoderAuthorizationErr         :"kVTVideoEncoderAuthorizationErr" ,
    kVTColorCorrectionPixelTransferFailedErr    :"kVTColorCorrectionPixelTransferFailedErr" ,
    kVTMultiPassStorageIdentifierMismatchErr    :"kVTMultiPassStorageIdentifierMismatchErr" ,
    kVTMultiPassStorageInvalidErr           :"kVTMultiPassStorageInvalidErr" ,
    kVTFrameSiloInvalidTimeStampErr         :"kVTFrameSiloInvalidTimeStampErr" ,
    kVTFrameSiloInvalidTimeRangeErr         :"kVTFrameSiloInvalidTimeRangeErr" ,
    kVTCouldNotFindTemporalFilterErr        :"kVTCouldNotFindTemporalFilterErr" ,
    kVTPixelTransferNotPermittedErr         :"kVTPixelTransferNotPermittedErr" ,
]


let naluTypesStrings : [String] =
    [
        "0: Unspecified (non-VCL)",
        "1: Coded slice of a non-IDR picture (VCL)", // P frame
        "2: Coded slice data partition A (VCL)",
        "3: Coded slice data partition B (VCL)",
        "4: Coded slice data partition C (VCL)",
        "5: Coded slice of an IDR picture (VCL)", // I frame
        "6: Supplemental enhancement information (SEI) (non-VCL)",
        "7: Sequence parameter set (non-VCL)", // SPS parameter
        "8: Picture parameter set (non-VCL)", // PPS parameter
        "9: Access unit delimiter (non-VCL)",
        "10: End of sequence (non-VCL)",
        "11: End of stream (non-VCL)",
        "12: Filler data (non-VCL)",
        "13: Sequence parameter set extension (non-VCL)",
        "14: Prefix NAL unit (non-VCL)",
        "15: Subset sequence parameter set (non-VCL)",
        "16: Reserved (non-VCL)",
        "17: Reserved (non-VCL)",
        "18: Reserved (non-VCL)",
        "19: Coded slice of an auxiliary coded picture without partitioning (non-VCL)",
        "20: Coded slice extension (non-VCL)",
        "21: Coded slice extension for depth view components (non-VCL)",
        "22: Reserved (non-VCL)",
        "23: Reserved (non-VCL)",
        "24: STAP-A Single-time aggregation packet (non-VCL)",
        "25: STAP-B Single-time aggregation packet (non-VCL)",
        "26: MTAP16 Multi-time aggregation packet (non-VCL)",
        "27: MTAP24 Multi-time aggregation packet (non-VCL)",
        "28: FU-A Fragmentation unit (non-VCL)",
        "29: FU-B Fragmentation unit (non-VCL)",
        "30: Unspecified (non-VCL)",
        "31: Unspecified (non-VCL)",
        "NAL_UNIT_VPS = 32",
        "NAL_UNIT_SPS = 33",
        "NAL_UNIT_PPS = 34",
        "NAL_UNIT_ACCESS_UNIT_DELIMITER = 35",
        "NAL_UNIT_EOS = 36",
        "NAL_UNIT_EOB = 37",
        "NAL_UNIT_FILLER_DATA = 38",
        "NAL_UNIT_PREFIX_SEI = 39",
        "NAL_UNIT_SUFFIX_SEI = 40",
        
        "NAL_UNIT_RESERVED_NVCL41 = 41",
        "NAL_UNIT_RESERVED_NVCL42 = 42",
        "NAL_UNIT_RESERVED_NVCL43 = 43",
        "NAL_UNIT_RESERVED_NVCL44 = 44",
        "NAL_UNIT_RESERVED_NVCL45 = 45",
        "NAL_UNIT_RESERVED_NVCL46 = 46",
        "NAL_UNIT_RESERVED_NVCL47 = 47",
        "NAL_UNIT_UNSPECIFIED_48 = 48",
        "NAL_UNIT_UNSPECIFIED_49 = 49",
        "NAL_UNIT_UNSPECIFIED_50 = 50",
        "NAL_UNIT_UNSPECIFIED_51 = 51",
        "NAL_UNIT_UNSPECIFIED_52 = 52",
        "NAL_UNIT_UNSPECIFIED_53 = 53",
        "NAL_UNIT_UNSPECIFIED_54 = 54",
        "NAL_UNIT_UNSPECIFIED_55 = 55",
        "NAL_UNIT_UNSPECIFIED_56 = 56",
        "NAL_UNIT_UNSPECIFIED_57 = 57",
        "NAL_UNIT_UNSPECIFIED_58 = 58",
        "NAL_UNIT_UNSPECIFIED_59 = 59",
        "NAL_UNIT_UNSPECIFIED_60 = 60",
        "NAL_UNIT_UNSPECIFIED_61 = 61",
        "NAL_UNIT_UNSPECIFIED_62 = 62",
        "NAL_UNIT_UNSPECIFIED_63 = 63",
        "NAL_UNIT_INVALID = 64"
]



//
//  RTPHeader.swift
//  Vapor
//
//  Created by 稲田成実 on 2018/07/23.
//  Copyright © 2018 Narumi Inada. All rights reserved.
//

import Foundation














struct PublishHeader {
    
    
    let data : Data
    let properties : [String:Any]
    
    
    init(from:Data) throws { // Init frequently
        data = from
        
        guard let props = try? JSONSerialization.jsonObject(with: data, options: []) as? [String:Any] else {
            throw AeError(desc: "PublishHeader init from data : properties can not decoded.")
        }
        
        properties = props
        
    }
}


/**
 Version 1 accesor
 */
protocol PublishHeader_v1 {
    
    static var versionKey : String {get}
    static var isIOSurfaceIDKey : String {get}
    static var timeStampKey: String {get}
    static var codecKey : String {get}
    
    var version : Float {get}
    var isIOSurfaceID : Bool {get}
    var pts : Double? {get}
    var codec : Codec {get}
    
    /** Version.1 initializer */
    init(version v:Float,isIOSurface isIO:Bool,pts ts:Double,codec c:Codec) throws
    
}

extension PublishHeader : PublishHeader_v1 {
    
    static let versionKey : String = "version"
    static let isIOSurfaceIDKey : String = "isIOSurfaceID"
    static let timeStampKey = "timeStamp"
    static let codecKey = "Codec"
    
    var version : Float { properties[PublishHeader.versionKey] as! Float }
    var isIOSurfaceID : Bool { properties[PublishHeader.isIOSurfaceIDKey] as! Bool }
    var pts : Double? { properties[PublishHeader.timeStampKey] as? Double }
    var codec : Codec { Codec.init(rawValue: properties[PublishHeader.codecKey] as! Int)! }
    
    init(version v:Float,isIOSurface isIO:Bool,pts ts:Double,codec c:Codec) throws { // Reusable
        
        properties = [Self.versionKey:v,
                      Self.isIOSurfaceIDKey:isIO,
                      Self.timeStampKey:ts,
                      Self.codecKey:c.rawValue]
        
        data = try JSONSerialization.data(withJSONObject: properties, options: [])
        
    }
}






struct RequestHeader {
    
    let versionKey : String = "version"
    let isSameIPKey : String = "isSameIP"
    
    let data : Data
    let properties : [String:Any]
    
    let version : Float
    let isSameIP : Bool
    
    init?(version v:Float,isSameIP ih:Bool) { // Reusable
        version = v
        isSameIP = ih
        properties = ["version":version,"isSameIP":isSameIP]
        do{
            data = try JSONSerialization.data(withJSONObject: properties, options: [])
        }catch{
            print(error)
            return nil
        }
        
    }
    
    init(from:Data) throws { // Init frequently
        data = from
        guard let props = try? JSONSerialization.jsonObject(with: data, options: []) as? [String:Any] else{
            throw AeError(desc: "RequestHeader init from data : properties can not decoded.")
        }
        
        properties = props
        if let v = properties["version"] as? Float {
            version = v
        }else{
            version = 0.0
        }
        
        if let v = properties["isSameIP"] as? Bool {
            isSameIP = v
        }else{
            isSameIP = false
        }
        
    }
    
}


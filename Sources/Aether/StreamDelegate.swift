//
//  Client.swift
//  CamSender
//
//  Created by 稲田成実 on 2018/07/05.
//  Copyright © 2018 Narumi Inada. All rights reserved.
//

 

import Foundation
import CoreMedia
import VideoToolbox



protocol AetherStreamCompletionDelegate {
    func outputComplete(sender:AetherStreamDelegate)
    func inputComplete(sender:AetherStreamDelegate, data: Data, header : Data, hasContinuousLoad:Bool)
    func inputInComplete(sender:AetherStreamDelegate)
    func errorOrCloseOccured(sender:AetherStreamDelegate)
}



final class AetherStreamDelegate : NSObject {
    let inputStream : InputStream
    let outputStream : OutputStream
    let delegate : AetherStreamCompletionDelegate
    
    
    var requesting : Bool = false
    var isSameIP : Bool = false
    var version : Float = 0.0
    func setParams(requesting rq:Bool, isSameIP sip:Bool,version v:Float){
        requesting = rq
        isSameIP = sip
        version = v
    }
    
    weak var runloop : RunLoop!
    init(input:InputStream,output:OutputStream,delegate del:AetherStreamCompletionDelegate) {
        inputStream = input
        outputStream = output
        delegate = del
        super.init()
        print("AetherStreamDelegate init")
        
        inputStream.delegate = self
        outputStream.delegate = self
        
        Thread{
            Thread.current.name = "AetherStreamDelegate"
            Thread.setThreadPriority(1.0)
            self.runloop = RunLoop.current
            
            self.inputStream.schedule(in: .current, forMode: trackingMode)
            self.inputStream.open()
            self.outputStream.schedule(in: .current, forMode: trackingMode)
            self.outputStream.open()
            
            self.runloop.run()
            print(Thread.current.name ?? "no name thread" + "closing")
            }.start()
    }
    
    deinit {
        close()
        print("AetherStream Delegate Deinit.")
    }
    
    var closed : Bool = false

    
    
    
    ///InputDelegate
    let data : UnsafeMutablePointer<UInt8> = UnsafeMutablePointer<UInt8>.allocate(capacity: bufSize)
    
    var receivingData : Data = Data()
    
    
    
    
    var header : Data = Data()
    
    
    
    ///OutputDelegate
    var sendingData : Data! = Data()
    var index : Int = 0
    var isProcessing : Bool = false
    
    var sendTime : Double = Date.timeIntervalSinceReferenceDate
    var receiveTime : Double = Date.timeIntervalSinceReferenceDate
    
    func close() {
        guard !closed else{
            print("was Closed")
            return
        }
        print("Closing...")
        
        closed = true
        
        
        self.inputStream.delegate = nil
        self.inputStream.close()
        
        
        
        self.outputStream.delegate = nil
        self.outputStream.close()
        
        
        
        
        self.inputStream.remove(from: self.runloop, forMode: trackingMode)
        self.outputStream.remove(from: self.runloop, forMode: trackingMode)
        
        
        CFRunLoopStop(runloop.getCFRunLoop())
        
        self.sendingData = nil
        self.runloop = nil
        print("AetherStreamDelegate Closing")
        
    }
    
    var inputState : InputState = .inactive(prefix: Data())
    
}

extension AetherStreamDelegate : StreamDelegate {
    func stream(_ aStream: Stream, handle eventCode: Stream.Event) {
        
        switch eventCode {
            
        case .openCompleted:
            
            break
        case .hasBytesAvailable://Input
            guard let st = aStream as? InputStream else{ break }
            readinputV2(st: st)
            //readInput(st:st)

            break
        case .hasSpaceAvailable://Output
            guard let st = aStream as? OutputStream else{ break }
            if isProcessing {
                doOutput(st: st)
            }
            break
        case .errorOccurred:
            
            close()
            print("ErrorOccured,output")
            break
        case .endEncountered:
            
            close()
            print("EndEncountered,output")
            break
        default:
            break
        }
        
    }
    
    ///Input
    
    enum InputState {
        case inactive(prefix:Data)
        case initializing(receivingData:Data)
        case initialized(headerSize:Int, frameSize:Int)
        case failed
    }
    
    
    
    private func readinputV2(st:InputStream){
        
        autoreleasepool {
            
            let fail : () -> Void = { [self] in inputState = .failed;}
            
            switch inputState {
            case .inactive(var prefix):
                let len = st.read(data, maxLength: 4 - prefix.count)
                guard len > 0 else{ fail(); break }
                prefix.append(data, count: len)
                
                if prefix.count == 4 {
                    
                    guard prefix[0] == 0x00 &&  prefix[1] == 0x00 && prefix[2] == 0x00 && prefix[3] == 0x02  else{ fail(); break }
                    inputState = .initializing(receivingData: Data())
                    
                }else{
                    inputState = .inactive(prefix: prefix)
                }
                
            case .initializing(var receiving):
                
                
                let targetSize = 16
                
                
                let readSize = targetSize - receiving.count
                
                let len = st.read(data, maxLength: readSize)
                guard len > 0 else{ fail(); break }
                
                receiving.append(data,count: len)
                
                if receiving.count == targetSize {
                    
                    let hsize = receiving.withUnsafeBytes { $0.load(as: Int64.self)}
                    let fsize = receiving.withUnsafeBytes { $0.load(fromByteOffset: 8,as: Int64.self) }
                    
                    inputState = .initialized(headerSize: Int(hsize), frameSize: Int(fsize))
                    
                }else{
                    inputState = .initializing(receivingData: receiving)
                }
                
                
                
            case .initialized(let headerSize, let frameSize):
                
                let targetSize : Int = Int(frameSize + headerSize)
                let readSize = min(bufSize, (targetSize - receivingData.count))
                
                let len = st.read(data, maxLength: readSize)
                guard len > 0 else{ fail(); break }
                
                self.receivingData.append(data, count: len)
                
                if self.receivingData.count == targetSize {
                    
                    self.header = self.receivingData[0..<headerSize]
                    self.receivingData.removeSubrange(0..<headerSize)
                    
                    self.complete(st: st)
                    
                    
                    
                }
                
                
                
            case .failed:
                while st.hasBytesAvailable {
                    st.read(data, maxLength: bufSize)
                }
                
                self.complete(st: st)
                
                break
            }
            
        }
        
        if st.hasBytesAvailable {
            readinputV2(st: st)
        }
        
    }
    
    
    
    
    
    func complete(st:InputStream){
        
        let failed : Bool = {if case .failed = inputState { return true }else{ return false } }()
        
                
        let tmp = receivingData
        let tmph = header
        
        self.receivingData = Data()
        self.inputState = .inactive(prefix: Data())
        
///        legacy
//        self.frameSize = -1
//        self.headerSize = -1
//
//        self.initialized = false
//        self.headerReadCompleted = false
        
        
        if failed {
            runloop.perform(inModes: performModes) {
                self.delegate.inputInComplete(sender: self)
            }
        }else{
            runloop.perform(inModes: performModes) {
                self.delegate.inputComplete(sender: self, data: tmp, header: tmph, hasContinuousLoad: st.hasBytesAvailable)
            }
        }
        
        
        
        //self.failed = false
        
    }
    
    
    ///Output
    

    
    func send(header href: Data, data: Data){
        guard runloop != nil else{ return }
        runloop.perform(inModes: performModes) {
            
            
            var headerSize : Int = href.count
            var frameSize : Int = data.count
            self.sendingData = Data([0x00,0x00,0x00,0x02])
            self.sendingData.append(contentsOf:  Data.init(bytes: &headerSize, count: MemoryLayout<Int64>.size) )
            self.sendingData.append(contentsOf:  Data.init(bytes: &frameSize, count: MemoryLayout<Int64>.size) )
            
            self.sendingData.append(contentsOf: href)
            
            self.sendingData.append(data)
            
            self.index = 0
            self.isProcessing = true
            
            
            
            if self.outputStream.hasSpaceAvailable {
                self.doOutput(st: self.outputStream)
            }
            
        }
        
        
    }
    
    func complete(stream:OutputStream){
        sendingData = Data()
        index = 0
        isProcessing = false
        delegate.outputComplete(sender: self)
    }
    
    
    
    
    @objc func doOutput(st:OutputStream){
        
        
        let maxLength = sendingData.count - index
        if maxLength > 0 {
            
            
            let maxIndex = min(index,sendingData.count - 1)
            let len : Int = sendingData.withUnsafeBytes{
                guard let p = $0.bindMemory(to: UInt8.self).baseAddress?.advanced(by: maxIndex) else{
                    return 0
                }
                return st.write( p , maxLength: maxLength)
            }
            
            
            
            if len > 0 {
                
                index += len
                if index >= sendingData.count {
                    self.complete(stream: st)
                    
                }
                
            }else{ // EOF or Err
                print("doOutput goes wrong:",len == -1 ? "Err" : "EOF")
            }
            
            
        }
        
        
        
    }
}






/**Reference*/
//extension AetherStreamDelegate { //Legacy
//
//
//    var initialized : Bool = false
//    var failed : Bool = false
//    var frameSize : Int = -1
//    var headerSize : Int = -1
//    var headerReadCompleted : Bool = false
//
//    private func readInput(st:InputStream){
//
//        guard !failed else{
//
//            while st.hasBytesAvailable { st.read(data, maxLength: bufSize) }
//
//            self.complete(st: st)
//            return
//        }
//
//        let len = st.read(data, maxLength: bufSize)
//
//        guard len > 0 else{
//            print("readInput err!")
//            delegate.errorOrCloseOccured(sender: self)
//            close()
//            return
//        }
//
//        receivingData.append(data, count: len)
//
//        if !self.initialized, self.receivingData.count >= 4 {
//
//            if self.receivingData[0] == 0x00 &&  self.receivingData[1] == 0x00 && self.receivingData[2] == 0x00 && self.receivingData[3] == 0x02 {
//                self.initialized = true
//
//                self.receivingData.removeSubrange(0..<4)
//            }else{
//                print("miss transport")
//                failed = true
//                while st.hasBytesAvailable {
//                    st.read(data, maxLength: bufSize)
//
//                }
//
//                self.complete(st: st)
//
//
//            }
//
//        }
//
//        if self.initialized, !st.hasBytesAvailable {
//
//
//            if self.headerSize == -1, self.receivingData.count >= MemoryLayout<Int>.size {
//
//                self.headerSize = self.receivingData.withUnsafeBytes({
//                    return $0.load(as: Int.self)
//                })
//
//                self.receivingData.removeSubrange(0..<MemoryLayout<Int>.size)
//                if self.headerSize == 0 {
//                    self.headerReadCompleted = true
//                }
//
//
//            }
//
//            if self.frameSize == -1, self.receivingData.count >= MemoryLayout<Int>.size {
//
//                self.frameSize = self.receivingData.withUnsafeBytes({
//                    return $0.load(as: Int.self)
//                })
//
//                self.receivingData.removeSubrange(0..<MemoryLayout<Int>.size)
//
//            }
//
//            if self.headerSize != -1, self.frameSize != -1 {
//
//                if !self.headerReadCompleted, self.receivingData.count >= self.headerSize, self.headerSize > 0 {
//                    self.header = self.receivingData[0..<self.headerSize]
//                    self.receivingData.removeSubrange(0..<self.headerSize)
//                    self.headerReadCompleted = true
//                }
//
//                if self.headerReadCompleted {
//
//                    if self.receivingData.count >= self.frameSize {
//                        self.complete(st: st)
//                    }
//                }
//            }
//        }
//    }
//}

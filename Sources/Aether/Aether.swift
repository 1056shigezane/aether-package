


//MARK:- General


let VERSION : Float = 0.1




struct AeError : Error {
    let desc : String
    var localizedDescription: String {return desc}
}

enum Codec : Int, Codable {
    case h264 = 0
    case hevc = 1
}






var debtime : Double = 0.0
var debool = true

func deb(label:String){
    if debool {
    let t = Date.timeIntervalSinceReferenceDate
        let dur = t - debtime
        print( (dur > 0.01 ? "!!!" : "") + "\(dur):\t" + label )
    debtime = t
    }
}


//MARK:- Misc

public extension Double {
    var cmTime : CMTime {
        return CMTime(seconds: self, preferredTimescale: 10000000)
    }
    
    var data : Data {
        var t = self
        return Data(bytes: &t, count: MemoryLayout<Double>.size)
    }
}



extension IOSurfaceID {
    var data : Data {
        var t = self
        return Data(bytes: &t, count: MemoryLayout<IOSurfaceID>.size)
    }
    
    init(from d:Data){
        self = d.withUnsafeBytes{
            $0.load(as: IOSurfaceID.self)
        }
    }
    
    var cvPixelBuffer : CVPixelBuffer? {
        guard let ref = IOSurfaceLookup(self) else{ return nil }
        
        var out : Unmanaged<CVPixelBuffer>?
        CVPixelBufferCreateWithIOSurface(kCFAllocatorDefault, ref, nil, &out)
       
        return out?.takeRetainedValue()
    }
}

import CoreMedia

extension CMSampleBuffer {
    
    func format () -> CMFormatDescription? {
        return CMSampleBufferGetFormatDescription(self)
    }
    
    var pts : CMTime {
        return CMSampleBufferGetPresentationTimeStamp(self)
    }
    
    func isDecodable() -> Bool {
        return CMSampleBufferGetNumSamples(self) > 0 &&
            CMSampleBufferGetSampleSize(self, at: 0) > 0 &&
            CMSampleBufferIsValid(self) &&
            CMSampleBufferDataIsReady(self)
    }
    
    func printAtrributes() {
        guard let attachmentsArray:CFArray = CMSampleBufferGetSampleAttachmentsArray(self, createIfNecessary: false) else{
            print("Attributes is Nil")
            return
        }
        
        let count = CFArrayGetCount(attachmentsArray)
        
        if count > 0 {
            
            print("SBuf has \(count) attributes.")
            
            for i in 0 ..< count {
                let dict = CFArrayGetValueAtIndex(attachmentsArray, i)
                let dictRef = unsafeBitCast(dict, to: CFDictionary.self) as Dictionary
                print(i+1,dictRef)
            }
        }
        print()
    }
    
    /**
     return Value, -1 = nil, 0 = false, 1 = true
     */
    
    func getPoperty(_ key:CFString) -> Int {
        guard let attachmentsArray:CFArray = CMSampleBufferGetSampleAttachmentsArray(self, createIfNecessary: false) else{
            return -1
        }
        
        if ( CFArrayGetCount(attachmentsArray) > 0 ) {
            let dict = CFArrayGetValueAtIndex(attachmentsArray, 0)
            let dictRef:CFDictionary = unsafeBitCast(dict, to: CFDictionary.self)
            
            guard let prop = CFDictionaryGetValue(dictRef , unsafeBitCast(key, to: UnsafeRawPointer.self))
                else {
                    return -1
            }
            return unsafeBitCast(prop, to: CFBoolean.self) != kCFBooleanFalse ? 1 : 0
            
        }else{
            return -1
        }
    }
    
    func isIFrame() -> Bool {
        let isNotSync = self.isNotSync()
        return isNotSync != 1
    }
    
    func isEarlierDisplayTimesAllowed() -> Int {
        return getPoperty(kCMSampleAttachmentKey_EarlierDisplayTimesAllowed)
    }
    
    func isDependsOnOthers() -> Int {
        return getPoperty(kCMSampleAttachmentKey_DependsOnOthers)
    }
    
    func isNotSync() -> Int {
        return getPoperty(kCMSampleAttachmentKey_NotSync)
    }
    
    func isPartialSync() -> Int {
        return getPoperty(kCMSampleAttachmentKey_PartialSync)
    }
    
    func isDependedOnByOthers() -> Int {
        return getPoperty(kCMSampleAttachmentKey_IsDependedOnByOthers)
    }
}


//func props(session:VTSession){
//    var p : CFDictionary? = nil
//
//    print(VTSessionCopySupportedPropertyDictionary(session, supportedPropertyDictionaryOut: &p)
//        == noErr ? "GotQOS" : "GetQOSErr")
//
//    guard let pl = p as? [CFString:Any] else{
//        print("No Properties")
//        return
//    }
//
//
//
//
//    print(pl)
//
//    for key in pl.keys {
//        if let d = pl[key] as? NSDictionary, let type = d["PropertyType"] as? String {
//
//            guard key != "iChatUsageString" as CFString,
//            key != "UserDPBFramesForFaceTime" as CFString,
//            key != "TransferFunction" as CFString,
//            key != "ColorPrimaries" as CFString
//            else{continue}
//
//            if type == "Number" {
//                var t : CFNumber = 0 as CFNumber
//                let st = VTSessionCopyProperty(session, key: key, allocator: kCFAllocatorMalloc, valueOut: &t)
//                guard st == noErr else{
//                    print(key, "fail.")
//                    continue
//                }
//
//                print(key,":" ,t)
//
//            }else if type == "Boolean" {
//                var t : CFBoolean = 0 as CFBoolean
//                let st = VTSessionCopyProperty(session, key: key, allocator: kCFAllocatorMalloc, valueOut: &t)
//                guard st == noErr else{
//                    print(key, "fail.")
//                    continue
//                }
//                print(key,":" ,t)
//            }else if type == "Enumeration" {
//                var t : CFArray? = nil
//                let st = VTSessionCopyProperty(session, key: key, allocator: kCFAllocatorMalloc, valueOut: &t)
//                guard st == noErr else{
//                    print(key, "fail.")
//                    continue
//                }
//                print(key,":" ,t!)
//            }
//
//
//
//        }
//
//    }
//
//
//    print("")
//
//
//    print("Session Props\n")
//
//
//}

//
//  File.swift
//  
//
//  Created by Narumi Inada on 2021/01/27.
//

import VideoToolbox

//MARK: - Compressor

final class H264Compressor : NSObject {
    
    var averageBitrate : Int = 10000000 {
        
        didSet{
            invalidateQueue.async {
                if self.compressionSession != nil {
                    let st = VTSessionSetProperty(self.compressionSession!, key: kVTCompressionPropertyKey_AverageBitRate, value: self.averageBitrate as CFTypeRef)
                    if st != noErr {
                        
                        print("VTSessionSetProperty Error Occurs,,", VTErrors[st] ?? "Undefined Err.")
                        
                    }
                }
            }
        }
    }
    
    
    
    var processSize : (width:Int,height:Int) = (0,0)
    var compressionSession : VTCompressionSession? = nil
    var delegate : H264ComplessionDelegate? = nil
    var lastPTS : CMTime = CMTime.zero
    let frameLimit : Bool = false
    
    public init(delegate del: H264ComplessionDelegate) {
        super.init()
        delegate = del
    }
    
    deinit {
        print("Compressor deinit")
        if compressionSession != nil {
            VTCompressionSessionCompleteFrames(compressionSession!, untilPresentationTimeStamp: CMTime.indefinite)
            VTCompressionSessionInvalidate(compressionSession!)
            compressionSession = nil
        }
    }
    
    func compress(imageBuffer pix:CVPixelBuffer, pts: CMTime? = nil){
        
        let width = CVPixelBufferGetWidth(pix)
        let height = CVPixelBufferGetHeight(pix)
        if processSize.width != width, processSize.height != height {
            
            createSessionH264(width: width, height: height)//Compression
            processSize = (width,height)
        }
        
        if compressionSession != nil {
            
            
            var ts = CMClockGetTime(CMClockGetHostTimeClock())
            if pts != nil {
                ts = pts!
            }
            
            lastPTS = ts
            
            
            
            
            VTCompressionSessionEncodeFrame(compressionSession!, imageBuffer: pix, presentationTimeStamp: ts, duration: .zero, frameProperties: nil , infoFlagsOut: nil, outputHandler: {
                (status,flags,sampleBuffer) in
                
                
                
                
                Thread.current.name = "Compression"
                if sampleBuffer != nil {
                    
                    self.delegate?.didCompress(sampleBuffer: sampleBuffer!)
                    
                }else{
                    
                    print("VTCompressionSession - cb - sampleBuffer is nil,",status,flags)
                    
                }
            })
        }
        
    }
    
    let invalidateQueue : DispatchQueue = DispatchQueue.init(label: "compressionInvalidateQ")
    
    func invalidate() {
        if compressionSession != nil {
            invalidateQueue.sync {
                VTCompressionSessionCompleteFrames(compressionSession!, untilPresentationTimeStamp: CMTime.positiveInfinity)
                VTCompressionSessionInvalidate(compressionSession!)
                compressionSession = nil
            }
        }
    }
    /*
    func createSession(width:Int,height:Int){
        
        invalidate()
        
        #if os(iOS)
        let specify = [:] as CFDictionary
        #elseif os(macOS)
        let specify = [
            kVTVideoEncoderSpecification_EnableHardwareAcceleratedVideoEncoder:kCFBooleanTrue,
           ] as CFDictionary
        #endif
        var status : OSStatus
        
      //  let bitrate : NSNumber = NSNumber(value: width * height * 15)
        
        
        
        let canHEVC = isHEVCHardwareEncodingSupported(width: width, height: height)
        
        let attrs : CFDictionary = [
            kVTCompressionPropertyKey_ColorPrimaries:kCVImageBufferColorPrimaries_ITU_R_709_2,
            kVTCompressionPropertyKey_TransferFunction:kCVImageBufferTransferFunction_ITU_R_709_2,
            kVTCompressionPropertyKey_YCbCrMatrix:kCVImageBufferYCbCrMatrix_ITU_R_709_2,
            
        //kVTCompressionPropertyKey_MaxKeyFrameInterval:30 as CFTypeRef,
        //kVTCompressionPropertyKey_PixelBufferPoolIsShared : kCFBooleanTrue!,
        //kVTCompressionPropertyKey_Depth: 24,
//        kVTCompressionPropertyKey_AllowFrameReordering:kCFBooleanTrue!,
//        kVTCompressionPropertyKey_PixelBufferPoolIsShared: kCFBooleanFalse!,
        //kVTCompressionPropertyKey_MaxFrameDelayCount : 0,
//        kVTCompressionPropertyKey_AverageBitRate : bitrate as NSNumber,
//        kVTCompressionPropertyKey_ExpectedFrameRate : mainFrameRate,
//        kVTCompressionPropertyKey_RealTime : kCFBooleanTrue!,
//        kVTCompressionPropertyKey_AllowTemporalCompression : kCFBooleanTrue!,
//        kVTCompressionPropertyKey_ProfileLevel : canHEVC ? kVTProfileLevel_HEVC_Main_AutoLevel : kVTProfileLevel_H264_High_5_2
        
        
        ] as CFDictionary
       
        
            
            status = VTCompressionSessionCreate(allocator: nil, width: Int32(width), height: Int32(height), codecType: kCMVideoCodecType_H264, encoderSpecification: specify, imageBufferAttributes: nil, compressedDataAllocator: nil, outputCallback: nil, refcon: nil, compressionSessionOut: &compressionSession)
        
        
        if status != noErr, compressionSession == nil {
            print("VTCompressionSessionCreate fail")
            return
        }
        
        VTSessionSetProperties(compressionSession!, propertyDictionary: attrs)
        
        processSize = (width,height)
        
        VTCompressionSessionPrepareToEncodeFrames(compressionSession!)
    }
    
    @available (iOS 11, *)
    func isHEVCHardwareEncodingSupported(width:Int,height:Int) -> Bool {
        let encoderSpecDict : [String : Any] =
            [kVTCompressionPropertyKey_ProfileLevel as String : kVTProfileLevel_HEVC_Main_AutoLevel,
             kVTCompressionPropertyKey_RealTime as String : true]

        let status = VTCopySupportedPropertyDictionaryForEncoder(width: Int32(width), height: Int32(height),
                                                                 codecType: kCMVideoCodecType_HEVC,
                                                                 encoderSpecification: encoderSpecDict as CFDictionary,
                                                                 encoderIDOut: nil, supportedPropertiesOut: nil)
        if status == kVTCouldNotFindVideoEncoderErr {
            print("Cant HEVC")
            return false
        }

        if status != noErr {
            return false
        }

        return true
    }
    */
    
    func createSessionH264(width:Int,height:Int){
        if compressionSession != nil {
            invalidateQueue.sync {
            VTCompressionSessionCompleteFrames(compressionSession!, untilPresentationTimeStamp: CMTime.indefinite)
            VTCompressionSessionInvalidate(compressionSession!)
            compressionSession = nil
            }
        }
        #if os(iOS)
        let specify =  [:] as CFDictionary
        #elseif os(macOS)
        let specify = [
            kVTVideoEncoderSpecification_EnableHardwareAcceleratedVideoEncoder:kCFBooleanTrue,
            ] as CFDictionary
        #endif
        
        let attrs : NSDictionary = [
            kVTCompressionPropertyKey_AverageBitRate : averageBitrate,
            kVTCompressionPropertyKey_RealTime : kCFBooleanTrue!,
            kVTCompressionPropertyKey_AllowTemporalCompression : kCFBooleanFalse!,
            kVTCompressionPropertyKey_AllowFrameReordering:kCFBooleanFalse!,
            kVTCompressionPropertyKey_ColorPrimaries:kCVImageBufferColorPrimaries_ITU_R_709_2,
            kVTCompressionPropertyKey_TransferFunction:kCVImageBufferTransferFunction_ITU_R_709_2,
            kVTCompressionPropertyKey_YCbCrMatrix:kCVImageBufferYCbCrMatrix_ITU_R_709_2,
            ]
        
        
        
        var status = VTCompressionSessionCreate(allocator: nil, width: Int32(width), height: Int32(height), codecType: kCMVideoCodecType_H264, encoderSpecification: specify, imageBufferAttributes: nil, compressedDataAllocator: nil, outputCallback: nil, refcon: nil, compressionSessionOut: &compressionSession)
        
        if status != noErr, compressionSession == nil {
            print("VTCompressionSessionCreate fail")
            return
        }
        
        status = VTSessionSetProperties(compressionSession!, propertyDictionary: attrs)
       
        
        if status != noErr, compressionSession == nil {
            print("VTCompressionSessionCreate Set Property failed.")
        }
        
        
        VTCompressionSessionPrepareToEncodeFrames(compressionSession!)
    }
    
    
    
    
    static func bufferToData(sbuf:CMSampleBuffer,isHEVC:Bool) -> (data:[FrameData],isKeyFrame:Bool) {
        
        var outputs : [FrameData] = []
        
        let isIFrame:Bool = sbuf.isIFrame()
        
        let pts = CMSampleBufferGetPresentationTimeStamp(sbuf)
        
        let nStartCode:[UInt8] = [0x00, 0x00, 0x00, 0x01]
        
        
        
        
        if isIFrame {
            
            
            let description:CMFormatDescription = CMSampleBufferGetFormatDescription(sbuf)!
            
            var numParams:size_t = 0
            
            if isHEVC {
                CMVideoFormatDescriptionGetHEVCParameterSetAtIndex(description, parameterSetIndex: 0, parameterSetPointerOut: nil, parameterSetSizeOut: nil, parameterSetCountOut: &numParams, nalUnitHeaderLengthOut: nil)
            }else{
                CMVideoFormatDescriptionGetH264ParameterSetAtIndex(description, parameterSetIndex: 0, parameterSetPointerOut: nil, parameterSetSizeOut: nil, parameterSetCountOut: &numParams, nalUnitHeaderLengthOut: nil)
            }
             let pp = sbuf
            
            var elementaryStream = Data()
            
            for i in 0 ..< numParams {
                var parameterSetPointer:UnsafePointer<UInt8>? = nil
                var parameterSetLength:size_t = 0
                var naluheadertLength:Int32 = 0
                if isHEVC {
                    CMVideoFormatDescriptionGetHEVCParameterSetAtIndex(description, parameterSetIndex: i, parameterSetPointerOut: &parameterSetPointer, parameterSetSizeOut: &parameterSetLength, parameterSetCountOut: nil, nalUnitHeaderLengthOut: &naluheadertLength)
                }else{
                    CMVideoFormatDescriptionGetH264ParameterSetAtIndex(description, parameterSetIndex: i, parameterSetPointerOut: &parameterSetPointer, parameterSetSizeOut: &parameterSetLength, parameterSetCountOut: nil, nalUnitHeaderLengthOut: &naluheadertLength)
                }
                
                
                
                elementaryStream.append(contentsOf: nStartCode)
                elementaryStream.append(Data(bytesNoCopy: UnsafeMutableRawPointer(mutating: parameterSetPointer!), count: parameterSetLength, deallocator: .custom {
                     p,s in
                     _ = pp
                     }))
                
            }
            outputs.append((elementaryStream,pts,true))
            
            
        }
        
        
        
        var blockBufferLength:size_t = 0
        var bufferDataPointer: UnsafeMutablePointer<Int8>? = nil
        
        CMBlockBufferGetDataPointer(CMSampleBufferGetDataBuffer(sbuf)!, atOffset: 0, lengthAtOffsetOut: nil, totalLengthOut: &blockBufferLength, dataPointerOut: &bufferDataPointer)
        
        if bufferDataPointer != nil {
            let pp = sbuf
            outputs.append((Data(bytesNoCopy: bufferDataPointer!, count: blockBufferLength, deallocator: Data.Deallocator.custom({ (p, s) in
                _ = pp /// Place Holder
            })),pts,false))
        }
        
        
        
        
        return (outputs,isIFrame)
        
        
    }
    
    
    
}




//
//  H264ElementaryStream.swift
//  CamSender
//
//  Created by 稲田成実 on 2018/07/10.
//  Copyright © 2018 Narumi Inada. All rights reserved.
//

import Foundation
import CoreMedia
import VideoToolbox








//MARK: - Decompressor

final class H264Decompressor : NSObject {
    
    
    
    var decompressionSession : VTDecompressionSession? = nil
    var formatDesc : CMVideoFormatDescription? = nil
    weak var delegate : H264DecompressionDelegate? = nil
    
    public override init() {
        formatDesc = nil
    }
    
    
    ///Takes 0.001 sec
    func streamToFrame(frame : Data,pts : CMTime? = nil) {

        var spsSize : Int = 0
        var ppsSize : Int = 0
        let startCodeIndex = 0
        var secondStartCodeIndex = 0


        var blockLength = 0
        var naluType = frame[startCodeIndex + 4] & 0x1f


        var status : OSStatus = 0

        if naluType != 7, formatDesc == nil {
            print("nalu not 7")
            return
        }

        if naluType == 7 {
            for i in startCodeIndex + 4 ..< startCodeIndex + 40 {

                if frame[i] == 0x00 && frame[i+1] == 0x00 && frame[i+2] == 0x00 && frame[i+3] == 0x01 {
                    secondStartCodeIndex = i
                    spsSize = secondStartCodeIndex
                    break
                }
            }

            naluType = frame[secondStartCodeIndex + 4] & 0x1F

        }

        if naluType == 8 {

            ppsSize = frame.count - spsSize

            let sps : UnsafeMutablePointer<UInt8> = .allocate(capacity: spsSize - 4)
            let pps : UnsafeMutablePointer<UInt8> = .allocate(capacity: ppsSize - 4)
            frame.copyBytes(to: sps, from: 4 ..< spsSize)
            frame.copyBytes(to: pps, from: spsSize + 4 ..< spsSize + ppsSize)
            let dpa = [UnsafePointer(sps),UnsafePointer(pps)]

            let sizeParamArray = [spsSize - 4,ppsSize - 4]
            
           
            var format : CMFormatDescription? = nil
            
            guard CMVideoFormatDescriptionCreateFromH264ParameterSets(allocator: kCFAllocatorMalloc, parameterSetCount: 2, parameterSetPointers: dpa, parameterSetSizes: sizeParamArray, nalUnitHeaderLength: 4, formatDescriptionOut: &format) == noErr,
                  let fmt = format
            else{
                print("Format Desc Create Error")
                return
            }
            
            
            
            guard decompressionSession != nil,
                  VTDecompressionSessionCanAcceptFormatDescription(decompressionSession!, formatDescription: formatDesc!)
                  
            else{
                createDecompSession(format: fmt)
                
                return
            }
            


            return

        }




        if naluType == 5 || naluType == 1 || naluType == 6, formatDesc != nil {//IDR

            var blockBuffer : CMBlockBuffer? = nil

            blockLength = frame.count


            status = frame.withUnsafeBytes{

                CMBlockBufferCreateWithMemoryBlock(allocator: nil, memoryBlock: UnsafeMutableRawPointer(mutating: $0.baseAddress), blockLength: blockLength, blockAllocator: kCFAllocatorNull, customBlockSource: nil, offsetToData: 0, dataLength: blockLength, flags: 0, blockBufferOut: &blockBuffer)

            }

            guard status == noErr else{
                print("H264Comp streamToFrame VTError:", VTErrors[status] ?? "noDesc", "\nNaluType:" + naluTypesStrings[Int(naluType)] ); return }

            var sampleBuffer : CMSampleBuffer? = nil



            let ts = pts ?? CMClockGetTime(CMClockGetHostTimeClock())


            let sampleSize = [blockLength]
            var timing : [CMSampleTimingInfo] = [CMSampleTimingInfo.init(duration: CMTime.invalid, presentationTimeStamp: ts, decodeTimeStamp: CMTime.invalid)]

            status = CMSampleBufferCreateReady(allocator: kCFAllocatorMalloc, dataBuffer: blockBuffer, formatDescription: formatDesc, sampleCount: 1, sampleTimingEntryCount: 1, sampleTimingArray: &timing[0], sampleSizeEntryCount: 1, sampleSizeArray: sampleSize, sampleBufferOut: &sampleBuffer)




            if status == noErr, decompressionSession != nil {


                VTDecompressionSessionDecodeFrame(decompressionSession!, sampleBuffer: sampleBuffer!, flags: [._EnableAsynchronousDecompression,._1xRealTimePlayback], infoFlagsOut: nil, outputHandler: {
                    (osstatus,infoflags,imageBuffer,pts,dts) in

                    guard let imageBuffer = imageBuffer else{
                        if osstatus != noErr {
                            print(VTErrors[osstatus] ?? "Err but no Desc", osstatus)
                        }else{
                            self.decompressionSession = nil
                        }
                        return }

                    self.delegate?.didDecompress(imageBuffer: imageBuffer,pts: pts)

                })
            }
        }

    }
    
    
    
    func setSampleBufferAttachments(_ sampleBuffer: CMSampleBuffer,dependsOnOthers:Bool) {
        let attachments: CFArray! = CMSampleBufferGetSampleAttachmentsArray(sampleBuffer, createIfNecessary: true)
        let dictionary = unsafeBitCast(CFArrayGetValueAtIndex(attachments, 0),
                                       to: CFMutableDictionary.self)
        let key = Unmanaged.passRetained(kCMSampleAttachmentKey_DisplayImmediately).toOpaque()
        let value = Unmanaged.passRetained(kCFBooleanTrue).toOpaque()
        CFDictionarySetValue(dictionary, key, value)
        
        let key1 = Unmanaged.passRetained(kCMSampleAttachmentKey_DependsOnOthers).toOpaque()
        let value1 = Unmanaged.passRetained(dependsOnOthers ? kCFBooleanFalse : kCFBooleanTrue).toOpaque()
        CFDictionarySetValue(dictionary, key1, value1)
        
        //if dependsOnOthers {
        let key2 = Unmanaged.passRetained(kCMSampleAttachmentKey_NotSync).toOpaque()
        let value2 = Unmanaged.passRetained(dependsOnOthers ? kCFBooleanFalse : kCFBooleanTrue).toOpaque()
        CFDictionarySetValue(dictionary, key2, value2)
        //}
        
        
    }
    
    
    
    
    
    
    
    func createDecompSession(format:CMFormatDescription){
        print("createDecompressionSession")
        
        formatDesc = nil
        
        if decompressionSession != nil {
            VTDecompressionSessionFinishDelayedFrames(decompressionSession!)
            VTDecompressionSessionWaitForAsynchronousFrames(decompressionSession!)
            VTDecompressionSessionInvalidate(decompressionSession!)
            decompressionSession = nil
        }
        
        
        
        
        
        let destinationPixelBufferAttributes = [kCVPixelBufferPixelFormatTypeKey:kCVPixelFormatType_444YpCbCr8BiPlanarFullRange,
                                                    //kCVPixelFormatType_444YpCbCr8,//kCVPixelFormatType_420YpCbCr8BiPlanarFullRange,
                                                kCVPixelBufferMetalCompatibilityKey:kCFBooleanTrue!] as CFDictionary
        
        
        #if os(iOS)
        let specify = [:] as CFDictionary
        #elseif os(macOS)
        let specify = [
            kVTVideoDecoderSpecification_EnableHardwareAcceleratedVideoDecoder:kCFBooleanTrue,
            ] as CFDictionary
        #endif
        
        
        
        guard noErr == VTDecompressionSessionCreate(allocator: nil, formatDescription: format, decoderSpecification: specify, imageBufferAttributes: destinationPixelBufferAttributes, outputCallback: nil, decompressionSessionOut: &decompressionSession) else{
            return
        }


        
        formatDesc = format
  
        
        let attrs = [kVTDecompressionPropertyKey_RealTime : kCFBooleanTrue!,
                  kVTCompressionPropertyKey_ColorPrimaries:kCVImageBufferColorPrimaries_ITU_R_709_2,
                kVTCompressionPropertyKey_TransferFunction:kCVImageBufferTransferFunction_ITU_R_709_2,
                     kVTCompressionPropertyKey_YCbCrMatrix:kCVImageBufferYCbCrMatrix_ITU_R_709_2,
                     //kVTDecompressionReorde
                     // kVTDecompressionPropertyKey_ThreadCount : 1
                     //kVTDecompressionPropertyKey_OutputPoolRequestedMinimumBufferCount : 0,
                     //kVTDecompressionPropertyKey_ThreadCount : 10,
                     //kVTDecompressionPropertyKey_ReducedCoefficientDecode : kCFBooleanTrue,
                     //kVTDecompressionPropertyKey_MaxOutputPresentationTimeStampOfFramesBeingDecoded : 20 as CFTypeRef,
                     //kVTDecompressionPropertyKey_MinOutputPresentationTimeStampOfFramesBeingDecoded : 0 as CFTypeRef,
                     
        ] as CFDictionary
        
        VTSessionSetProperties(decompressionSession!, propertyDictionary: attrs)
        
        
    }
    
    func setQualityOfService(){
        
        
        
        let tier1 = [kVTDecompressionPropertyKey_OnlyTheseFrames:kVTDecompressionProperty_OnlyTheseFrames_AllFrames,
                     kVTDecompressionPropertyKey_ReducedFrameDelivery:1.0 as CFNumber ] as CFDictionary
        let tier2 = [kVTDecompressionPropertyKey_OnlyTheseFrames:kVTDecompressionProperty_OnlyTheseFrames_AllFrames,
                     kVTDecompressionPropertyKey_ReducedFrameDelivery:0.5 as CFNumber ] as CFDictionary
        let tier3 = [kVTDecompressionPropertyKey_OnlyTheseFrames:kVTDecompressionProperty_OnlyTheseFrames_IFrames,
                     kVTDecompressionPropertyKey_ReducedFrameDelivery:0.25 as CFNumber ] as CFDictionary
        let qos = [kVTDecompressionPropertyKey_SuggestedQualityOfServiceTiers:[tier1,tier2,tier3]] as CFDictionary
            
        
        let st = VTSessionSetProperties(decompressionSession!, propertyDictionary: qos)
        print("DecompQOSSet",VTErrors[st] ?? "ErrNotFound")
        
        var p : CFDictionary? = nil
        
        print(VTSessionCopySupportedPropertyDictionary(decompressionSession!, supportedPropertyDictionaryOut: &p)
            == noErr ? "GotQOS" : "GetQOSErr")
        
        print(p ?? "nilでした")
    }
    
    deinit {
        print("Decompressor deinit")
        formatDesc = nil
        if decompressionSession != nil {
            VTDecompressionSessionFinishDelayedFrames(decompressionSession!)
            VTDecompressionSessionWaitForAsynchronousFrames(decompressionSession!)
            VTDecompressionSessionInvalidate(decompressionSession!)
            decompressionSession = nil
        }
    }
    
    
    /**
     
     ProtoTyping
     
     */
    /*
    func streamToHEVCFrame(frame : Data, pts : CMTime? = nil) {
        
        
        var blockLength = 0
        
        var nextStartIndex = 0
        
        var naluType = frame[nextStartIndex + 4]// & 0x1f
        
        
        var status : OSStatus = 0
        
        
        print("nalu is",naluType)
        if naluType != 64, formatDesc == nil {
            //error
            if naluTypesStrings.count < naluType {
                print("type:",naluTypesStrings[Int(naluType)])
            }
            print(frame)
            return
        }
        
        
        
        var tmpPArray : [[UInt8]] = []
        
        
        
        while naluType == 64 || naluType == 66 || naluType == 68 {
            
            
            var parameterSize : Int = 0
            let startIndex = nextStartIndex
            var broken : Bool = false
            for i in startIndex + 4 ... frame.count {
                
                if i + 3 >= frame.count {
                    
                    parameterSize = frame.count - startIndex - 4
                    nextStartIndex = i + 3
                    naluType = 200
                    broken = true
                    break
                    
                }else if frame[i] == 0x00 && frame[i+1] == 0x00 && frame[i+2] == 0x00 && frame[i+3] == 0x01 {
                    
                    parameterSize = i - startIndex - 4
                    nextStartIndex = i
                    naluType = frame[nextStartIndex + 4]
                    
                    broken = true
                    break
                    
                }
            }
            
            print("nalu is",naluType)
            
            print("broken",broken)
            
            
            if parameterSize > 0 {
                
                var parameter = [UInt8].init(repeating: 0, count: parameterSize)
                frame.copyBytes(to: &parameter, from: startIndex + 4 ..< nextStartIndex )

                tmpPArray.append(parameter)
                
            }
            
            
            if nextStartIndex >= frame.count {
                
                
                
                var format : CMFormatDescription? = nil
                
                var dataParamArray : [UnsafePointer<UInt8>] = []
                var sizeParamArray : [Int] = []
                
                
                for v in tmpPArray {
                    v.withUnsafeBufferPointer { ptr in
                        dataParamArray.append(ptr.baseAddress!)
                    }
                    
                    sizeParamArray.append(v.count)
                }
                
                guard CMVideoFormatDescriptionCreateFromHEVCParameterSets(allocator: kCFAllocatorMalloc, parameterSetCount: dataParamArray.count, parameterSetPointers: dataParamArray, parameterSetSizes: sizeParamArray, nalUnitHeaderLength: 4, extensions: nil, formatDescriptionOut: &format) == noErr,
                      let fmt = format
                else{
                    print("Format Desc Create Error",status)
                    return
                }
                
                
                
                tmpPArray = []
                
                
                
                
                guard decompressionSession != nil,
                   VTDecompressionSessionCanAcceptFormatDescription(decompressionSession!, formatDescription: fmt)
                else{
                    createDecompSession(format: fmt)
                    return
                }
                
                
                return
            }
        }
        
        
        
        
        if formatDesc != nil {//IDR
            
            var blockBuffer : CMBlockBuffer? = nil
            
            blockLength = frame.count
            
            
          
            
            status = frame.withUnsafeBytes{
                CMBlockBufferCreateWithMemoryBlock(allocator: nil, memoryBlock: UnsafeMutableRawPointer(mutating: $0.baseAddress), blockLength: blockLength, blockAllocator: kCFAllocatorNull, customBlockSource: nil, offsetToData: 0, dataLength: blockLength, flags: 0, blockBufferOut: &blockBuffer)
            }
            
            var sampleBuffer : CMSampleBuffer? = nil
            
            if status == noErr {
                
                let ts = pts ?? CMClockGetTime(CMClockGetHostTimeClock())
                let sampleSize = [blockLength]
                var timing : [CMSampleTimingInfo] = [CMSampleTimingInfo.init(duration: CMTime.invalid, presentationTimeStamp: ts, decodeTimeStamp: CMTime.invalid)]
                
                
                status = CMSampleBufferCreateReady(allocator: kCFAllocatorMalloc, dataBuffer: blockBuffer, formatDescription: formatDesc, sampleCount: 1, sampleTimingEntryCount: 1, sampleTimingArray: &timing[0], sampleSizeEntryCount: 1, sampleSizeArray: sampleSize, sampleBufferOut: &sampleBuffer)
                
            }else{
                print("~~~~~~~ Received NALU Type \"%@\" ~~~~~~~~", naluTypesStrings[Int(naluType)])
                return
            }
            
            if status == noErr, decompressionSession != nil {
                
                
                
                
                
                
                VTDecompressionSessionDecodeFrame(decompressionSession!, sampleBuffer: sampleBuffer!, flags: [._EnableAsynchronousDecompression,._EnableTemporalProcessing,._1xRealTimePlayback], infoFlagsOut: nil, outputHandler: {
                    
                    [weak self] (osstatus,infoflags,imageBuffer,pts,dts) in
                    Thread.setThreadPriority(1.0)
                    
                    if imageBuffer != nil {
                        self?.delegate?.didDecompress(imageBuffer: imageBuffer!,pts: pts)
                    }
                    
                    
                    
                })
                
                
                
            }
        }
        
        
        
    }
     */
    
}


